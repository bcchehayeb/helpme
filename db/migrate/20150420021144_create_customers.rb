class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :name
      t.integer :phone, :limit => 10
      t.string :email
      t.integer :credit_score

      t.timestamps null: false
    end
  end
end
