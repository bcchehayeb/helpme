class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.integer :year
      t.string :make
      t.string :model
      t.string :style
      t.float :msrp
      t.string :vin
      t.boolean :available

      t.timestamps null: false
    end
  end
end
