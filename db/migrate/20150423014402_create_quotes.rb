class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.integer :customer_id
      t.integer :vehicle_id
      t.integer :employee_id
      t.float :price
      t.float :tax
      t.float :total
      t.boolean :sold

      t.timestamps null: false
    end
  end
end
