# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Customer.delete_all
Position.delete_all
Employee.delete_all
Vehicle.delete_all
Quote.delete_all

Customer.create(name: "Luke Skywalker", phone: 7135037675, email: "luke.skywalker@jedi.com", credit_score: 794)
Customer.create(name: "Obi-Wan Kenobi", phone: 7134090201, email: "obi@jedi.com", credit_score: 755)
Customer.create(name: "Han Solo", phone: 7131234231, email: "han.solo@rebels.com", credit_score: 547)
Customer.create(name: "R2 D2", phone: 7137354309, email: "r2d2@jedi.com", credit_score: 789)
Customer.create(name: "Jar-Jar Binks", phone: 7132435852, email: "jarjar@rebels.com", credit_score: 204)
Customer.create(name: "Qui-Gon Jinn", phone: 7133278988, email: "qui@jedi.com", credit_score: 772)

Position.create(title: "Dealership Owner")
Position.create(title: "Inventory Manager")
Position.create(title: "Finance Manager")
Position.create(title: "Sales Manager")
Position.create(title: "Sales Person")

Employee.create(name: "The Emperor", phone: 8324561983, email: "emperor@sith.com", position_id: 1)
Employee.create(name: "Darth Vader", phone: 8329081734, email: "d.vader@sith.com", position_id: 2)
Employee.create(name: "Darth Maul", phone: 8326470265, email: "darth.maul@sith.com", position_id: 3)
Employee.create(name: "Count Dooku", phone: 8326457211, email: "count.dooku@sith.com", position_id: 4)
Employee.create(name: "Boba Fett", phone: 832165243, email: "boba.fett@republic.com", position_id: 5, manager_id: 4)
Employee.create(name: "Storm Trooper", phone: 8321426453, email: "storm.trooper@republic.com", position_id: 5, manager_id: 4)

Vehicle.create(year: 4044, make: "Rebels", model: "X-Wing", style: "Coupe", msrp: 250000, vin: "FBVA142360914X1", available: true)
Vehicle.create(year: 4038, make: "Rebels", model: "A-Wing", style: "Coupe", msrp: 125000, vin: "FBVA142360914X2", available: true)
Vehicle.create(year: 4044, make: "Rebels", model: "AAT Battletank", style: "Sedan", msrp: 450000, vin: "TBVA142360914X1", available: true)
Vehicle.create(year: 4046, make: "Republic", model: "DDT Tank", style: "Sedan", msrp: 550000, vin: "TBVA142360914X2", available: true)
Vehicle.create(year: 4042, make: "Republic", model: "Droid Gunship", style: "Sedan", msrp: 775000, vin: "SBVA142360914X1", available: true)
Vehicle.create(year: 4051, make: "Republic", model: "Jeddi Starfighter", style: "Coupe", msrp: 250000, vin: "FBVA142360914X3", available: true)
Vehicle.create(year: 4038, make: "Republic", model: "Imperial Star Destroyer", style: "Sedan", msrp: 1000000, vin: "SBVA142360914X2", available: true)
Vehicle.create(year: 4044, make: "Republic", model: "Tie Fighter", style: "Coupe", msrp: 250000, vin: "FBVA142360914X4", available: true)

Quote.create(customer_id: 1, vehicle_id: 1, employee_id: 5, price: 275000, tax: 11825, total: 286825, sold: true)
Quote.create(customer_id: 2, vehicle_id: 2, employee_id: 6, price: 137000, tax: 5891, total: 142891, sold: false)
Quote.create(customer_id: 2, vehicle_id: 2, employee_id: 5, price: 137000, tax: 5891, total: 142891, sold: false)
Quote.create(customer_id: 3, vehicle_id: 2, employee_id: 5, price: 137000, tax: 5891, total: 142891, sold: false)
Quote.create(customer_id: 3, vehicle_id: 4, employee_id: 6, price: 605000, tax: 26015, total: 631015, sold: true)
Quote.create(customer_id: 4, vehicle_id: 5, employee_id: 6, price: 852500, tax: 36657.5, total: 889157.5, sold: false)
Quote.create(customer_id: 4, vehicle_id: 5, employee_id: 5, price: 852500, tax: 36657.5, total: 889157, sold: false)
Quote.create(customer_id: 4, vehicle_id: 5, employee_id: 6, price: 852500, tax: 36657.5, total: 889157, sold: true)
