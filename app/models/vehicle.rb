class Vehicle < ActiveRecord::Base

  has_many :quotes


  def show_available
    if available?
      "Yes"
    else
      "No"
    end
  end
end
