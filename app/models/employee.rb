class Employee < ActiveRecord::Base

  belongs_to :position
  has_many :quotes

  has_many :employees, class_name: "Employee", foreign_key: "manager_id"
  belongs_to :manager, class_name: "Employee"

  def manager_name
    if self.manager.nil?
      'N/A'
    else
      self.manager.name
    end
  end
end
