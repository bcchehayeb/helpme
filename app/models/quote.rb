class Quote < ActiveRecord::Base

  belongs_to :customer
  belongs_to :employee
  belongs_to :vehicle

  after_save :update_vehicle





#update the vehicle availability if the quote is sold.
  def update_vehicle
    if self.sold == true
      Vehicle.update(self.vehicle_id, :available => false)

      q = Quote.find(Quote.where("vehicle_id = ? AND sold = ?", self.vehicle_id, false).pluck(:id))
      q.each do |x|
        x.destroy
      end
    end
  end

  #total all quote totals for those quotes that have been marked as sold.
  def self.gross_revenue
    gross_revenue = 0
    Quote.all.each do |q|
      if q.sold?
        gross_revenue += q.total
      end
    end
    gross_revenue
  end

  def self.net_profit
    net_profit = 0
    Quote.all.each do |q|
      if q.sold?
        net_profit += (q.price * 0.10)
      end
    end
    net_profit
  end

  def self.total_sales
    total_sales = 0
    Quote.all.each do |q|
      if q.sold?
        total_sales += q.tax
      end
    end
    total_sales
  end

  def show_sold
    if sold?
      "Yes"
    else
      "No"
    end
  end
end
