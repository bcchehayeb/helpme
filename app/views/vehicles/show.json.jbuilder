json.extract! @vehicle, :id, :year, :make, :model, :style, :msrp, :vin, :available, :created_at, :updated_at
