json.array!(@vehicles) do |vehicle|
  json.extract! vehicle, :id, :year, :make, :model, :style, :msrp, :vin, :available
  json.url vehicle_url(vehicle, format: :json)
end
