json.array!(@quotes) do |quote|
  json.extract! quote, :id, :customer_id, :vehicle_id, :employee_id, :price, :tax, :total, :sold
  json.url quote_url(quote, format: :json)
end
