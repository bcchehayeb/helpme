json.array!(@customers) do |customer|
  json.extract! customer, :id, :name, :phone, :email, :credit_score
  json.url customer_url(customer, format: :json)
end
